import dotenv from 'dotenv';
import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import cors from 'cors';
import { Request, Response, NextFunction } from 'express';
import Routes from './routes';

dotenv.config();
const app = express();

// Set the environment
app.set('env', process.env.NODE_ENV || 'development');
app.set('port', process.env.PORT || 3000);

// Cors
app.use(
  cors({
    origin: '*',
    credentials: true,
  })
);

// Middleware
app.use(logger('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// Routes
app.use('/', Routes);

// Route not found handler
app.use((req: Request, res: Response, next: NextFunction) => {
  const err = new Error(`${req.method} ${req.url} Not Found`);
  next(err);
});

// Error handler
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  console.error(err);
  res.status(err.status || 500);
  res.json({
    error: {
      message: err.message,
    },
  });
});

let server: any;

export default {
  app,
  start() {
    if (server) {
      return;
    }
    return new Promise(resolve => {
      server = app.listen(app.get('port'));
      server.on('listening', () => {
        const addr = server.address();
        const bind =
          typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
        console.log(
          `Express Server Port: ${bind} | Environment : ${app.get('env')}`
        );
        resolve(server);
      });
    });
  },
  stop() {
    return server.close();
  },
};
